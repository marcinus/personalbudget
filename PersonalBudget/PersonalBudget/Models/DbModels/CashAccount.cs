﻿using PersonalBudget.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PersonalBudget.Models.DbModels
{
    public class CashAccount
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Saldo początkowe")]
        public decimal StartBalance { get; set; }
        [Required]
        [ForeignKey("Owner")]
        public string OwnerID { get; set; }

        public virtual ApplicationUser Owner { get; set; }
        public virtual ICollection<CashTransaction> Transactions { get; set; }

        [Display(Name = "Suma przychodów")]
        public decimal TotalIncome
        {
            get
            {
                return Transactions.Where(x => x.TargetAccountID == ID).Select(x => x.Transaction.Amount).Sum();
            }
        }

        [Display(Name = "Suma wydatków")]
        public decimal TotalOutcome
        {
            get
            {
                return Transactions.Where(x => x.SourceAccountID == ID).Select(x => x.Transaction.Amount).Sum();
            }
        }

        [Display(Name = "Bilans")]
        public decimal CurrentBalance
        {
            get
            {
                return StartBalance + TotalIncome - TotalOutcome;
            }
        }

        public static CashAccount CreateFromViewModel(AccountCreateViewModel model)
        {
            return new CashAccount
            {
                Name = model.Name,
                StartBalance = Math.Round(model.StartBalance, 2)
            };
        }

        public void UpdateByViewModel(AccountUpdateViewModel model)
        {
            Name = model.Name;
            StartBalance = Math.Round(model.StartBalance, 2);
        }
    }
}