﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PersonalBudget.Models.DbModels
{
    public class Category
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }
        [ForeignKey("ParentCategory")]
        public int? ParentCategoryID { get; set; }
        [Required]
        [ForeignKey("RecordOwner")]
        public string RecordOwnerID { get; set; }

        [Display(Name = "Kategoria nadrzędna")]
        public virtual Category ParentCategory { get; set; }
        public virtual List<Category> ChildCategories { get; set; }
        public virtual ApplicationUser RecordOwner { get; set; }

        public virtual List<Product> Products { get; set; }

        [Display(Name = "Całkowita kwota tylko w tej kategorii")]
        public decimal TotalAmountOwn
        {
            get
            {
                return Products.Select(x => x.TotalAmount).Sum();
            }
        }

        [Display(Name = "Całkowita kwota w poddrzewie")]
        public decimal TotalAmount
        {
            get
            {
                return ChildCategories.Select(x => x.TotalAmount).Sum() + TotalAmountOwn;
            }
        }
    }
}