﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalBudget.Models.DbModels
{
    public class Transaction
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public DateTime? Date { get; set; }
        [ForeignKey("SourceSubject")]
        public int SourceSubjectID { get; set; }
        [ForeignKey("TargetSubject")]
        public int TargetSubjectID { get; set; }

        [Required]
        [ForeignKey("RecordOwner")]
        public string RecordOwnerID { get; set; }

        public virtual Subject SourceSubject { get; set; }
        public virtual Subject TargetSubject { get; set; }

        public virtual ApplicationUser RecordOwner { get; set; }
    }
}