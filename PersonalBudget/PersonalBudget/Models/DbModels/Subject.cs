﻿using PersonalBudget.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalBudget.Models.DbModels
{
    public class Subject
    {
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        
        [ForeignKey("EntryOwner")]
        public string EntryOwnerID { get; set; }

        [ForeignKey("Address")]
        public int AddressID { get; set; }

        [ForeignKey("RealSubject")]
        public int? RealSubjectID { get; set; }

        public virtual ApplicationUser EntryOwner { get; set; }
        public virtual Address Address { get; set; }
        public virtual Subject RealSubject { get; set; }

        public virtual List<ApplicationUser> AssociatedRealUsers { get; set; }

        public virtual List<Subject> AssociatedRealSubjects { get; set; }

        public static Subject CreateFromViewModel(SubjectAndAddressCreateViewModel model)
        {
            return new Subject
            {
                Name = model.Name,
                Address = new Address
                {
                    Street = model.Street,
                    PlaceNumber = model.PlaceNumber,
                    PostalCode = model.PostalCode,
                    City = model.City,
                    Country = model.Country
                }
            };
        }

        public void UpdateByViewModel(SubjectAndAddressUpdateViewModel model)
        {
            Name = model.Name;
            Address.Street = model.Street;
            Address.PlaceNumber = model.PlaceNumber;
            Address.PostalCode = model.PostalCode;
            Address.City = model.City;
            Address.Country = model.Country;
        }
    }
}