﻿using PersonalBudget.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PersonalBudget.Models.DbModels
{
    public class Bill
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        [ForeignKey("IssuerSubject")]
        public int IssuerSubjectID { get; set; }
        [Required]
        [ForeignKey("RecipientSubject")]
        public int RecipientSubjectID { get; set; }

        [Required]
        [Display(Name = "Data wystawienia")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Data dodania")]
        public DateTime InsertionDate { get; set; }

        [Required]
        [ForeignKey("RecordOwner")]
        public string RecordOwnerID { get; set; }

        public virtual ApplicationUser RecordOwner { get; set; }
        [Display(Name = "Wystawca")]
        public virtual Subject IssuerSubject { get; set; }
        [Display(Name = "Odbiorca")]
        public virtual Subject RecipientSubject { get; set; }
        public virtual List<BillEntry> BillEntries { get; set; }

        [Display(Name = "Kwota")]
        public decimal Amount
        {
            get
            {
                return BillEntries.Select(x => Policies.RoundInBillEntry(x.Quantity * x.UnitPrice)).Sum();
            }
        }
    }
}