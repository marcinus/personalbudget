﻿using System.Collections.Generic;

namespace PersonalBudget.Models.DbModels
{
    public class Address
    {
        public int ID { get; set; }
        public string Street { get; set; }
        public string PlaceNumber { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public virtual List<Subject> Subjects { get; set; }
    }
}