﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PersonalBudget.Models.DbModels
{
    public class Product
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [ForeignKey("Category")]
        public int? CategoryID { get; set; }

        [Required]
        public string Name { get; set; }

        public decimal NominalQuantity { get; set; }
        public string Unit { get; set; }

        [Required]
        [ForeignKey("RecordOwner")]
        public string RecordOwnerID { get; set; }

        public virtual Category Category { get; set; }
        public virtual List<BillEntry> BillEntries { get; set; }
        public virtual ApplicationUser RecordOwner { get; set; }

        public decimal TotalQuantity
        {
            get
            {
                return BillEntries.Select(x => x.Quantity).Sum();
            }
        }

        public decimal TotalAmount
        {
            get
            {
                return BillEntries.Select(x => Math.Floor(x.Quantity * x.UnitPrice * 100) / 100).Sum();
            }
        }
    }
}