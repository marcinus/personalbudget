﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalBudget.Models.DbModels
{
    public class CashlessTransaction
    {
        [Key]
        [Required]
        [ForeignKey("Transaction")]
        public int TransactionID { get; set; }

        [ForeignKey("SourceAccount")]
        public int SourceAccountID { get; set; }
        [ForeignKey("TargetAccount")]
        public int TargetAccountID { get; set; }

        public virtual Transaction Transaction { get; set; }

        public virtual CashlessAccount SourceAccount { get; set; }
        public virtual CashlessAccount TargetAccount { get; set; }
    }
}