﻿using PersonalBudget.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalBudget.Models.DbModels
{
    public class BillEntry
    {
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        [ForeignKey("Bill")]
        public int BillID { get; set; }

        [Required]
        [ForeignKey("Product")]
        public int ProductID { get; set; }

        public string Description { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public decimal Quantity { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public decimal UnitPrice { get; set; }

        [Required]
        [ForeignKey("RecordOwner")]
        public string RecordOwnerID { get; set; }

        public virtual Bill Bill { get; set; }
        public virtual Product Product { get; set; }

        public virtual ApplicationUser RecordOwner { get; set; }

        public decimal Amount
        {
            get
            {
                return Policies.RoundPrice(Quantity * UnitPrice);
            }
        }
    }
}