﻿using PersonalBudget.Models.DbModels;
using System.ComponentModel.DataAnnotations;

namespace PersonalBudget.Models.ViewModels
{
    public abstract class BasicSubjectAndAddressViewModel
    {
        [Display(Name = "Nazwa")]
        [Required(ErrorMessage = "Należy podać nazwę podmiotu")]
        public string Name { get; set; }
        [Display(Name = "Ulica")]
        public string Street { get; set; }
        [Display(Name = "Nr domu")]
        public string PlaceNumber { get; set; }
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }
        [Display(Name = "Miasto")]
        public string City { get; set; }
        [Display(Name = "Kraj")]
        public string Country { get; set; }
    }

    public class SubjectAndAddressCreateViewModel : BasicSubjectAndAddressViewModel
    {

    }

    public class SubjectAndAddressUpdateViewModel : BasicSubjectAndAddressViewModel
    {
        [Required]
        public int SubjectID { get; set; }

        public static SubjectAndAddressUpdateViewModel FromModel(Subject subject)
        {
            return new SubjectAndAddressUpdateViewModel
            {
                SubjectID = subject.ID,
                Name = subject.Name,
                Street = subject.Address.Street,
                PlaceNumber = subject.Address.PlaceNumber,
                PostalCode = subject.Address.PostalCode,
                City = subject.Address.City,
                Country = subject.Address.Country
            };
        }
    }
}