﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PersonalBudget.Models.ViewModels
{
    public abstract class BaseBillViewModel
    {
        [Display(Name = "Wystawca")]
        [Required(ErrorMessage = "Należy podać wystawcę rachunku")]
        public int IssuerSubjectID { get; set; }
        [Display(Name = "Odbiorca")]
        [Required(ErrorMessage = "Należy podać odbiorcę rachunku")]
        public int RecipientSubjectID { get; set; }

        [Display(Name = "Data wystawienia")]
        [Required(ErrorMessage = "Należy podać datę wystawienia rachunku")]
        public DateTime Date { get; set; }

        public List<SelectListItem> EligibleSubjects { get; set; }
        [Display(Name = "Kategoria")]
        public List<SelectListItem> Categories { get; set; }
    }

    public abstract class BaseBillEntryViewModel
    {
        public int ProductID { get; set; }

        [Display(Name = "Nazwa produktu")]
        public string ProductName { get; set; }

        [Display(Name = "Uwagi")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Ilość")]
        [Range(0, int.MaxValue)]
        public decimal Quantity { get; set; }

        [Required]
        [Display(Name = "Cena jednostkowa [zł]")]
        [Range(0, int.MaxValue)]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Kwota całkowita")]
        public decimal TotalPrice { get; set; }

        [Display(Name = "Kategoria")]
        public int? CategoryID { get; set; }
    }

    public class CreateBillEntryViewModel : BaseBillEntryViewModel
    {

    }

    public class UpdateBillEntryViewModel : BaseBillEntryViewModel
    {
        public int? BillEntryID { get; set; }
    }

    public class CreateBillViewModel : BaseBillViewModel
    {
        public List<CreateBillEntryViewModel> BillEntries { get; set; }
    }

    public class UpdateBillViewModel : BaseBillViewModel
    {
        [Required]
        public int BillID { get; set; }
        public List<UpdateBillEntryViewModel> BillEntries { get; set; }
    }
}