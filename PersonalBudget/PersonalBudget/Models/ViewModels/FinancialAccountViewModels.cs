﻿using PersonalBudget.Models.DbModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PersonalBudget.Models.ViewModels
{
    public class AccountSummaryViewModel
    {
        public List<CashAccount> CashAccounts { get; set; }
        public List<CashlessAccount> CashlessAccounts { get; set; }

        [Display(Name = "Bilans kont gotówkowych")]
        public decimal CashAccountsTotalBalance
        {
            get
            {
                return CashAccounts.Select(x => x.CurrentBalance).Sum();
            }
        }

        [Display(Name = "Bilans kont bezgotówkowych")]
        public decimal CashlessAccountsTotalBalance
        {
            get
            {
                return CashlessAccounts.Select(x => x.CurrentBalance).Sum();
            }
        }
    }

    public abstract class AccountBaseCRUDViewModel
    {

        [Required(ErrorMessage = "Nazwa konta jest wymagana")]
        [Display(Name = "Nazwa konta")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Podanie początkowego bilansu konta jest wymagane")]
        [Display(Name = "Początkowy bilans konta")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:0.00}", NullDisplayText = "")]
        public decimal StartBalance { get; set; } = 0.0m;
    }

    public class AccountCreateViewModel : AccountBaseCRUDViewModel
    {
    }

    public class AccountUpdateViewModel : AccountBaseCRUDViewModel
    {
        [Required]
        public int AccountID { get; set; }

        public static AccountUpdateViewModel FromModel(CashAccount account)
        {
            return new AccountUpdateViewModel
            {
                AccountID = account.ID,
                Name = account.Name,
                StartBalance = account.StartBalance
            };
        }

        public static AccountUpdateViewModel FromModel(CashlessAccount account)
        {
            return new AccountUpdateViewModel
            {
                AccountID = account.ID,
                Name = account.Name,
                StartBalance = account.StartBalance
            };
        }
    }
}