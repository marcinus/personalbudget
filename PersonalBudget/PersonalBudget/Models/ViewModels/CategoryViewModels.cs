﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PersonalBudget.Models.ViewModels
{
    public class BaseCategoryViewModel
    {
        [Display(Name = "Nazwa kategorii")]
        public string Name { get; set; }
        [Display(Name = "Kategoria nadrzędna")]
        public int? ParentCategoryID { get; set; }

        public List<SelectListItem> EligibleCategories { get; set; }
    }

    public class CreateCategoryViewModel : BaseCategoryViewModel
    {

    }

    public class UpdateCategoryViewModel : BaseCategoryViewModel
    {
        [Required]
        public int CategoryID { get; set; }
    }
}