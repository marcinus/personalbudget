namespace PersonalBudget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReleaseRequiredDependencies : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Products", "CategoryID", "public.Categories");
            DropIndex("public.Products", new[] { "CategoryID" });
            DropIndex("public.Categories", new[] { "ParentCategoryID" });
            AlterColumn("public.Products", "CategoryID", c => c.Int());
            AlterColumn("public.Categories", "ParentCategoryID", c => c.Int());
            CreateIndex("public.Products", "CategoryID");
            CreateIndex("public.Categories", "ParentCategoryID");
            AddForeignKey("public.Products", "CategoryID", "public.Categories", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("public.Products", "CategoryID", "public.Categories");
            DropIndex("public.Categories", new[] { "ParentCategoryID" });
            DropIndex("public.Products", new[] { "CategoryID" });
            AlterColumn("public.Categories", "ParentCategoryID", c => c.Int(nullable: false));
            AlterColumn("public.Products", "CategoryID", c => c.Int(nullable: false));
            CreateIndex("public.Categories", "ParentCategoryID");
            CreateIndex("public.Products", "CategoryID");
            AddForeignKey("public.Products", "CategoryID", "public.Categories", "ID", cascadeDelete: true);
        }
    }
}
