namespace PersonalBudget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BillsProductsCategoriesScaffolding : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Subjects", "RealUserID", "public.AspNetUsers");
            DropForeignKey("public.Subjects", "EntryOwnerID", "public.AspNetUsers");
            DropIndex("public.Subjects", new[] { "EntryOwnerID" });
            DropIndex("public.Subjects", new[] { "RealUserID" });
            CreateTable(
                "public.BillEntries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BillID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        Description = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RecordOwnerID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Bills", t => t.BillID, cascadeDelete: true)
                .ForeignKey("public.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("public.AspNetUsers", t => t.RecordOwnerID, cascadeDelete: true)
                .Index(t => t.BillID)
                .Index(t => t.ProductID)
                .Index(t => t.RecordOwnerID);
            
            CreateTable(
                "public.Bills",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IssuerSubjectID = c.Int(nullable: false),
                        RecipientSubjectID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        InsertionDate = c.DateTime(nullable: false),
                        RecordOwnerID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Subjects", t => t.IssuerSubjectID, cascadeDelete: true)
                .ForeignKey("public.Subjects", t => t.RecipientSubjectID, cascadeDelete: true)
                .ForeignKey("public.AspNetUsers", t => t.RecordOwnerID, cascadeDelete: true)
                .Index(t => t.IssuerSubjectID)
                .Index(t => t.RecipientSubjectID)
                .Index(t => t.RecordOwnerID);
            
            CreateTable(
                "public.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CategoryID = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        NominalQuantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Unit = c.String(),
                        RecordOwnerID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("public.AspNetUsers", t => t.RecordOwnerID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.RecordOwnerID);
            
            CreateTable(
                "public.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ParentCategoryID = c.Int(nullable: false),
                        RecordOwnerID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Categories", t => t.ParentCategoryID)
                .ForeignKey("public.AspNetUsers", t => t.RecordOwnerID, cascadeDelete: true)
                .Index(t => t.ParentCategoryID)
                .Index(t => t.RecordOwnerID);
            
            AddColumn("public.Subjects", "RealSubjectID", c => c.Int());
            AddColumn("public.AspNetUsers", "OwnSubjectID", c => c.Int(nullable: false));
            AddColumn("public.AspNetUsers", "Subject_ID", c => c.Int());
            AddColumn("public.Transactions", "RecordOwnerID", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("public.Subjects", "EntryOwnerID", c => c.String(maxLength: 128));
            CreateIndex("public.Subjects", "EntryOwnerID");
            CreateIndex("public.Subjects", "RealSubjectID");
            CreateIndex("public.AspNetUsers", "OwnSubjectID");
            CreateIndex("public.AspNetUsers", "Subject_ID");
            CreateIndex("public.Transactions", "RecordOwnerID");
            AddForeignKey("public.AspNetUsers", "OwnSubjectID", "public.Subjects", "ID", cascadeDelete: true);
            AddForeignKey("public.AspNetUsers", "Subject_ID", "public.Subjects", "ID");
            AddForeignKey("public.Subjects", "RealSubjectID", "public.Subjects", "ID");
            AddForeignKey("public.Transactions", "RecordOwnerID", "public.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("public.Subjects", "EntryOwnerID", "public.AspNetUsers", "Id");
            DropColumn("public.Subjects", "RealUserID");
        }
        
        public override void Down()
        {
            AddColumn("public.Subjects", "RealUserID", c => c.String(maxLength: 128));
            DropForeignKey("public.Subjects", "EntryOwnerID", "public.AspNetUsers");
            DropForeignKey("public.Transactions", "RecordOwnerID", "public.AspNetUsers");
            DropForeignKey("public.BillEntries", "RecordOwnerID", "public.AspNetUsers");
            DropForeignKey("public.BillEntries", "ProductID", "public.Products");
            DropForeignKey("public.Products", "RecordOwnerID", "public.AspNetUsers");
            DropForeignKey("public.Products", "CategoryID", "public.Categories");
            DropForeignKey("public.Categories", "RecordOwnerID", "public.AspNetUsers");
            DropForeignKey("public.Categories", "ParentCategoryID", "public.Categories");
            DropForeignKey("public.BillEntries", "BillID", "public.Bills");
            DropForeignKey("public.Bills", "RecordOwnerID", "public.AspNetUsers");
            DropForeignKey("public.Bills", "RecipientSubjectID", "public.Subjects");
            DropForeignKey("public.Bills", "IssuerSubjectID", "public.Subjects");
            DropForeignKey("public.Subjects", "RealSubjectID", "public.Subjects");
            DropForeignKey("public.AspNetUsers", "Subject_ID", "public.Subjects");
            DropForeignKey("public.AspNetUsers", "OwnSubjectID", "public.Subjects");
            DropIndex("public.Transactions", new[] { "RecordOwnerID" });
            DropIndex("public.Categories", new[] { "RecordOwnerID" });
            DropIndex("public.Categories", new[] { "ParentCategoryID" });
            DropIndex("public.Products", new[] { "RecordOwnerID" });
            DropIndex("public.Products", new[] { "CategoryID" });
            DropIndex("public.Bills", new[] { "RecordOwnerID" });
            DropIndex("public.Bills", new[] { "RecipientSubjectID" });
            DropIndex("public.Bills", new[] { "IssuerSubjectID" });
            DropIndex("public.BillEntries", new[] { "RecordOwnerID" });
            DropIndex("public.BillEntries", new[] { "ProductID" });
            DropIndex("public.BillEntries", new[] { "BillID" });
            DropIndex("public.AspNetUsers", new[] { "Subject_ID" });
            DropIndex("public.AspNetUsers", new[] { "OwnSubjectID" });
            DropIndex("public.Subjects", new[] { "RealSubjectID" });
            DropIndex("public.Subjects", new[] { "EntryOwnerID" });
            AlterColumn("public.Subjects", "EntryOwnerID", c => c.String(nullable: false, maxLength: 128));
            DropColumn("public.Transactions", "RecordOwnerID");
            DropColumn("public.AspNetUsers", "Subject_ID");
            DropColumn("public.AspNetUsers", "OwnSubjectID");
            DropColumn("public.Subjects", "RealSubjectID");
            DropTable("public.Categories");
            DropTable("public.Products");
            DropTable("public.Bills");
            DropTable("public.BillEntries");
            CreateIndex("public.Subjects", "RealUserID");
            CreateIndex("public.Subjects", "EntryOwnerID");
            AddForeignKey("public.Subjects", "EntryOwnerID", "public.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("public.Subjects", "RealUserID", "public.AspNetUsers", "Id");
        }
    }
}
