namespace PersonalBudget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnhancingAccountModels : DbMigration
    {
        public override void Up()
        {
            RenameColumn("public.CashAccounts", "CurrentBalance", "StartBalance");
            RenameColumn("public.CashlessAccounts", "CurrentBalance", "StartBalance");
            AddColumn("public.CashAccounts", "OwnerID", c => c.String(nullable: false, maxLength: 128));
            AddColumn("public.CashlessAccounts", "OwnerID", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("public.CashAccounts", "OwnerID");
            CreateIndex("public.CashlessAccounts", "OwnerID");
            AddForeignKey("public.CashAccounts", "OwnerID", "public.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("public.CashlessAccounts", "OwnerID", "public.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            RenameColumn("public.CashAccounts", "StartBalance", "CurrentBalance");
            RenameColumn("public.CashlessAccounts", "StartBalance", "CurrentBalance");
            DropForeignKey("public.CashlessAccounts", "OwnerID", "public.AspNetUsers");
            DropForeignKey("public.CashAccounts", "OwnerID", "public.AspNetUsers");
            DropIndex("public.CashlessAccounts", new[] { "OwnerID" });
            DropIndex("public.CashAccounts", new[] { "OwnerID" });
            DropColumn("public.CashlessAccounts", "OwnerID");
            DropColumn("public.CashAccounts", "OwnerID");
        }
    }
}
