namespace PersonalBudget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubjectAndAddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.Addresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        PlaceNumber = c.String(),
                        PostalCode = c.String(),
                        City = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "public.Subjects",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        EntryOwnerID = c.String(nullable: false, maxLength: 128),
                        AddressID = c.Int(nullable: false),
                        RealUserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Addresses", t => t.AddressID, cascadeDelete: true)
                .ForeignKey("public.AspNetUsers", t => t.EntryOwnerID, cascadeDelete: true)
                .ForeignKey("public.AspNetUsers", t => t.RealUserID)
                .Index(t => t.EntryOwnerID)
                .Index(t => t.AddressID)
                .Index(t => t.RealUserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.Subjects", "RealUserID", "public.AspNetUsers");
            DropForeignKey("public.Subjects", "EntryOwnerID", "public.AspNetUsers");
            DropForeignKey("public.Subjects", "AddressID", "public.Addresses");
            DropIndex("public.Subjects", new[] { "RealUserID" });
            DropIndex("public.Subjects", new[] { "AddressID" });
            DropIndex("public.Subjects", new[] { "EntryOwnerID" });
            DropTable("public.Subjects");
            DropTable("public.Addresses");
        }
    }
}
