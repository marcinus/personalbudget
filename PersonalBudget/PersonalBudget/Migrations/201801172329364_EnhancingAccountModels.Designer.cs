// <auto-generated />
namespace PersonalBudget.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class EnhancingAccountModels : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EnhancingAccountModels));
        
        string IMigrationMetadata.Id
        {
            get { return "201801172329364_EnhancingAccountModels"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
