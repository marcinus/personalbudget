namespace PersonalBudget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransactionBasics : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.CashAccounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CurrentBalance = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "public.CashTransactions",
                c => new
                    {
                        TransactionID = c.Int(nullable: false),
                        SourceAccountID = c.Int(nullable: false),
                        TargetAccountID = c.Int(nullable: false),
                        CashAccount_ID = c.Int(),
                    })
                .PrimaryKey(t => t.TransactionID)
                .ForeignKey("public.CashAccounts", t => t.SourceAccountID, cascadeDelete: true)
                .ForeignKey("public.CashAccounts", t => t.TargetAccountID, cascadeDelete: true)
                .ForeignKey("public.Transactions", t => t.TransactionID)
                .ForeignKey("public.CashAccounts", t => t.CashAccount_ID)
                .Index(t => t.TransactionID)
                .Index(t => t.SourceAccountID)
                .Index(t => t.TargetAccountID)
                .Index(t => t.CashAccount_ID);
            
            CreateTable(
                "public.Transactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Single(nullable: false),
                        Date = c.DateTime(),
                        SourceSubjectID = c.Int(nullable: false),
                        TargetSubjectID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("public.Subjects", t => t.SourceSubjectID, cascadeDelete: true)
                .ForeignKey("public.Subjects", t => t.TargetSubjectID, cascadeDelete: true)
                .Index(t => t.SourceSubjectID)
                .Index(t => t.TargetSubjectID);
            
            CreateTable(
                "public.CashlessAccounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CurrentBalance = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "public.CashlessTransactions",
                c => new
                    {
                        TransactionID = c.Int(nullable: false),
                        SourceAccountID = c.Int(nullable: false),
                        TargetAccountID = c.Int(nullable: false),
                        CashlessAccount_ID = c.Int(),
                    })
                .PrimaryKey(t => t.TransactionID)
                .ForeignKey("public.CashlessAccounts", t => t.SourceAccountID, cascadeDelete: true)
                .ForeignKey("public.CashlessAccounts", t => t.TargetAccountID, cascadeDelete: true)
                .ForeignKey("public.Transactions", t => t.TransactionID)
                .ForeignKey("public.CashlessAccounts", t => t.CashlessAccount_ID)
                .Index(t => t.TransactionID)
                .Index(t => t.SourceAccountID)
                .Index(t => t.TargetAccountID)
                .Index(t => t.CashlessAccount_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.CashlessTransactions", "CashlessAccount_ID", "public.CashlessAccounts");
            DropForeignKey("public.CashlessTransactions", "TransactionID", "public.Transactions");
            DropForeignKey("public.CashlessTransactions", "TargetAccountID", "public.CashlessAccounts");
            DropForeignKey("public.CashlessTransactions", "SourceAccountID", "public.CashlessAccounts");
            DropForeignKey("public.CashTransactions", "CashAccount_ID", "public.CashAccounts");
            DropForeignKey("public.CashTransactions", "TransactionID", "public.Transactions");
            DropForeignKey("public.Transactions", "TargetSubjectID", "public.Subjects");
            DropForeignKey("public.Transactions", "SourceSubjectID", "public.Subjects");
            DropForeignKey("public.CashTransactions", "TargetAccountID", "public.CashAccounts");
            DropForeignKey("public.CashTransactions", "SourceAccountID", "public.CashAccounts");
            DropIndex("public.CashlessTransactions", new[] { "CashlessAccount_ID" });
            DropIndex("public.CashlessTransactions", new[] { "TargetAccountID" });
            DropIndex("public.CashlessTransactions", new[] { "SourceAccountID" });
            DropIndex("public.CashlessTransactions", new[] { "TransactionID" });
            DropIndex("public.Transactions", new[] { "TargetSubjectID" });
            DropIndex("public.Transactions", new[] { "SourceSubjectID" });
            DropIndex("public.CashTransactions", new[] { "CashAccount_ID" });
            DropIndex("public.CashTransactions", new[] { "TargetAccountID" });
            DropIndex("public.CashTransactions", new[] { "SourceAccountID" });
            DropIndex("public.CashTransactions", new[] { "TransactionID" });
            DropTable("public.CashlessTransactions");
            DropTable("public.CashlessAccounts");
            DropTable("public.Transactions");
            DropTable("public.CashTransactions");
            DropTable("public.CashAccounts");
        }
    }
}
