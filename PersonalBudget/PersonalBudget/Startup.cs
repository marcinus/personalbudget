﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PersonalBudget.Startup))]
namespace PersonalBudget
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
