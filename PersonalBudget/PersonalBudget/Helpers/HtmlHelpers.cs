﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace PersonalBudget.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString TextEditorGroupFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            string htmlString = "<div class=\"form-group\">";
            htmlString += html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" });
            htmlString += "<div class=\"col-md-10\">";
            htmlString += html.EditorFor(expression, new { htmlAttributes = new { @class = "form-control" } });
            htmlString += html.ValidationMessageFor(expression, "", new { @class = "text-danger" });
            htmlString += "</div>";
            htmlString += "</div>";

            return new MvcHtmlString(htmlString);
        }

        public static MvcHtmlString DropDownListGroupFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> selectList)
        {
            string htmlString = "<div class=\"form-group\">";
            htmlString += html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" });
            htmlString += "<div class=\"col-md-10\">";
            htmlString += html.DropDownListFor(expression, selectList, new { htmlAttributes = new { @class = "form-control" } });
            htmlString += html.ValidationMessageFor(expression, "", new { @class = "text-danger" });
            htmlString += "</div>";
            htmlString += "</div>";

            return new MvcHtmlString(htmlString);
        }

        public static MvcHtmlString Lookup<T, TReturn>(this HtmlHelper<T> html, Expression<Func<T, TReturn>> expression)
        {
            return MvcHtmlString.Create(ExpressionHelper.GetExpressionText(expression));
        }

        public static MvcHtmlString CustomNamedEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string customFieldName)
        {
            return html.EditorFor(expression, new { htmlAttributes = new { @class = "form-control", name = customFieldName } });
        }

    }
}