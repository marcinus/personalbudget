﻿using System;

namespace PersonalBudget.Helpers
{
    public class Policies
    {
        public static decimal RoundInBillEntry(decimal amount)
        {
            var temporary = Math.Floor(amount*100);
            return temporary / 100;
        }

        public static decimal RoundQuantity(decimal amount)
        {
            var temporary = Math.Floor(amount * 1000);
            return temporary / 1000;
        }

        public static decimal RoundPrice(decimal amount)
        {
            var temporary = Math.Floor(amount * 100);
            return temporary / 100;
        }
    }
}