﻿using PersonalBudget.Controllers.Abstract;
using PersonalBudget.Models.DbModels;
using PersonalBudget.Models.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace PersonalBudget.Controllers
{
    [Authorize]
    public class FinancialAccountController : AbstractDatabaseController
    {
        // GET: FinancialAccount
        public ActionResult Index()
        {
            AccountSummaryViewModel model = new AccountSummaryViewModel()
            {
                CashAccounts = Db.CashAccounts.Where(x => x.OwnerID == UserID).ToList(),
                CashlessAccounts = Db.CashlessAccounts.Where(x => x.OwnerID == UserID).ToList()
            };
            return View(model);
        }

        #region CashAccounts

        public ActionResult CreateCashAccount()
        {
            AccountCreateViewModel model = new AccountCreateViewModel();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCashAccount([Bind(Include = "Name,StartBalance")] AccountCreateViewModel model)
        {
            if(ModelState.IsValid)
            {
                CashAccount account = CashAccount.CreateFromViewModel(model);
                account.OwnerID = UserID;
                Db.CashAccounts.Add(account);
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        
        public ActionResult EditCashAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CashAccount account = Db.CashAccounts.Find(id);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            return View(AccountUpdateViewModel.FromModel(account));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCashAccount([Bind(Include = "AccountID,Name,StartBalance")] AccountUpdateViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CashAccount account = Db.CashAccounts.Find(model.AccountID);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                account.UpdateByViewModel(model);

                Db.Entry(account).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }
        
        public ActionResult DeleteCashAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashAccount account = Db.CashAccounts.Find(id);
            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(account);
        }
        
        [HttpPost, ActionName("DeleteCashAccount")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCashAccount(int id)
        {
            CashAccount account = Db.CashAccounts.Find(id);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            Db.CashAccounts.Remove(account);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region CashlessAccounts

        public ActionResult CreateCashlessAccount()
        {
            AccountCreateViewModel model = new AccountCreateViewModel();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCashlessAccount([Bind(Include = "Name,StartBalance")]AccountCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                CashlessAccount account = CashlessAccount.CreateFromViewModel(model);
                account.OwnerID = UserID;
                Db.CashlessAccounts.Add(account);
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult EditCashlessAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CashlessAccount account = Db.CashlessAccounts.Find(id);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            return View(AccountUpdateViewModel.FromModel(account));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCashlessAccount([Bind(Include = "AccountID,Name,StartBalance")] AccountUpdateViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CashlessAccount account = Db.CashlessAccounts.Find(model.AccountID);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                account.UpdateByViewModel(model);

                Db.Entry(account).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult DeleteCashlessAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashlessAccount account = Db.CashlessAccounts.Find(id);
            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        [HttpPost, ActionName("DeleteCashlessAccount")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCashlessAccount(int id)
        {
            CashlessAccount account = Db.CashlessAccounts.Find(id);

            if (account == null || account.OwnerID != UserID)
            {
                return HttpNotFound();
            }

            Db.CashlessAccounts.Remove(account);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion
    }
}