﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PersonalBudget.Models;
using PersonalBudget.Models.DbModels;
using PersonalBudget.Controllers.Abstract;
using PersonalBudget.Models.ViewModels;
using System;
using System.Collections.Generic;
using PersonalBudget.Helpers;

namespace PersonalBudget.Controllers
{
    [Authorize]
    public class BillsController : AbstractDatabaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Bills
        public ActionResult Index()
        {
            var bills = db.Bills.Where(b => b.RecordOwnerID == UserID).Include(b => b.IssuerSubject).Include(b => b.RecipientSubject).Include(b => b.RecordOwner).Include(b => b.BillEntries);
            return View(bills.ToList());
        }

        // GET: Bills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
            if (bill == null || bill.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        private List<SelectListItem> GetEligibleSubjects()
        {
            var eligibleSubjects = Db.Subjects.Where(x => (x.EntryOwnerID == UserID || x.EntryOwnerID == null) && x.ID != UserData.OwnSubjectID);
            var eligibleSubjectsList = eligibleSubjects.Select(x => new SelectListItem { Text = x.Name, Value = x.ID.ToString() }).ToList();
            return eligibleSubjectsList;
        }

        private List<SelectListItem> GetCategories()
        {
            var categories = Db.Categories.Where(x => x.RecordOwnerID == UserID || x.RecordOwnerID == null);
            var categoriesList = categories.Select(x => new SelectListItem { Text = x.Name, Value = x.ID.ToString() }).ToList();
            return categoriesList;
        }

        // GET: Bills/Create
        public ActionResult Create()
        {
            var model = new CreateBillViewModel() { Date = DateTime.Now, EligibleSubjects = GetEligibleSubjects(), Categories = GetCategories() };
            return View(model);
        }

        // POST: Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IssuerSubjectID,Date,BillEntries")] CreateBillViewModel model)
        {
            var IssuerSubject = Db.Subjects.Find(model.IssuerSubjectID);
            if (IssuerSubject == null || IssuerSubject.EntryOwnerID != null && IssuerSubject.EntryOwnerID != UserID)
            {
                ModelState.AddModelError(string.Empty, "Podany wystawca rachunku jest nieprawidłowy");
            }
            if(model.BillEntries == null || model.BillEntries.Any(b => string.IsNullOrEmpty(b.ProductName)))
            {
                ModelState.AddModelError("", "Jeden lub więcej produktów nie ma uzupełnionej nazwy.");
            }
            if (ModelState.IsValid)
            {
                Bill bill = new Bill
                {
                    RecordOwnerID = UserID,
                    InsertionDate = DateTime.Now,
                    RecipientSubjectID = UserData.OwnSubjectID,
                    Date = model.Date,
                    IssuerSubjectID = model.IssuerSubjectID
                };
                foreach (CreateBillEntryViewModel entryModel in model.BillEntries)
                {
                    BillEntry entry = new BillEntry
                    {
                        RecordOwnerID = UserID,
                        Bill = bill,
                        Quantity = Policies.RoundQuantity(entryModel.Quantity),
                        UnitPrice = Policies.RoundPrice(entryModel.UnitPrice),
                        Product = new Product
                        {
                            Name = entryModel.ProductName,
                            RecordOwnerID = UserID,
                            CategoryID = entryModel.CategoryID  // TODO
                        },
                    };
                    db.BillEntries.Add(entry);
                }
                db.Bills.Add(bill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            model.EligibleSubjects = GetEligibleSubjects();
            model.Categories = GetCategories();
            return View(model);
        }

        // GET: Bills/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            UpdateBillViewModel model = new UpdateBillViewModel
            {
                BillID = bill.ID,
                Categories = GetCategories(),
                EligibleSubjects = GetEligibleSubjects(),
                Date = bill.Date,
                IssuerSubjectID = bill.IssuerSubjectID,
                RecipientSubjectID = bill.RecipientSubjectID,
                BillEntries = bill.BillEntries.Select(x => new UpdateBillEntryViewModel
                {
                    BillEntryID = x.ID,
                    ProductName = x.Product.Name,
                    CategoryID = x.Product.CategoryID,
                    UnitPrice = x.UnitPrice,
                    Quantity = x.Quantity,
                    ProductID = x.ProductID
                }).ToList()
            };
            return View(model);
        }

        // POST: Bills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BillID,IssuerSubjectID,Date,BillEntries")] UpdateBillViewModel model)
        {
            var IssuerSubject = db.Subjects.Find(model.IssuerSubjectID);
            if (IssuerSubject == null || IssuerSubject.EntryOwnerID != null && IssuerSubject.EntryOwnerID != UserID)
            {
                ModelState.AddModelError(string.Empty, "Podany wystawca rachunku jest nieprawidłowy");
            }
            Bill bill = db.Bills.Find(model.BillID);
            if (ModelState.IsValid && bill != null)
            {
                bill.IssuerSubjectID = model.IssuerSubjectID;
                bill.Date = model.Date;
                List<BillEntry> billEntriesToRemove = new List<BillEntry>();
                foreach (BillEntry entry in db.BillEntries.Where(x => x.BillID == bill.ID))
                {
                    if(!model.BillEntries.Any(x => x.BillEntryID == entry.ID))
                    {
                        billEntriesToRemove.Add(entry);
                    }
                }
                foreach (UpdateBillEntryViewModel entryModel in model.BillEntries)
                {
                    BillEntry entry = db.BillEntries.Find(entryModel.BillEntryID);
                    if(entry == null)
                    {
                        entry = new BillEntry
                        {
                            RecordOwnerID = UserID,
                            Bill = bill,
                            Quantity = Policies.RoundQuantity(entryModel.Quantity),
                            UnitPrice = Policies.RoundPrice(entryModel.UnitPrice),
                            Product = new Product
                            {
                                Name = entryModel.ProductName,
                                RecordOwnerID = UserID,
                                CategoryID = entryModel.CategoryID  // TODO
                            },
                        };
                        db.BillEntries.Add(entry);
                    }
                    else
                    {
                        entry.UnitPrice = entryModel.UnitPrice;
                        entry.Quantity = entryModel.Quantity;
                        entry.Product.CategoryID = entryModel.CategoryID;
                        entry.Product.Name = entryModel.ProductName;
                        db.Entry(entry).State = EntityState.Modified;
                    }
                }
                foreach(BillEntry entry in billEntriesToRemove)
                {
                    db.BillEntries.Remove(entry);
                }
                db.Entry(bill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            model.EligibleSubjects = GetEligibleSubjects();
            model.Categories = GetCategories();
            return View(model);
        }

        // GET: Bills/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
            if (bill == null || bill.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Bills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bill bill = db.Bills.Find(id);
            if (bill == null || bill.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            db.Bills.Remove(bill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
