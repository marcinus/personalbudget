﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PersonalBudget.Models;
using PersonalBudget.Models.DbModels;
using PersonalBudget.Controllers.Abstract;
using PersonalBudget.Models.ViewModels;

namespace PersonalBudget.Controllers
{
    [Authorize]
    public class CategoriesController : AbstractDatabaseController
    {

        // GET: Categories
        public ActionResult Index()
        {
            var categories = Db.Categories.Where(c => c.RecordOwnerID == UserID).Include(c => c.ParentCategory).Include(c => c.RecordOwner);
            return View(categories.ToList());
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = Db.Categories.Find(id);
            if (category == null || category.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        public List<SelectListItem> GetCategories()
        {
            var eligibleCategories = Db.Categories.Where(x => x.RecordOwner == null || x.RecordOwnerID == UserID);
            var eligibleCategoriesList = eligibleCategories.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.ID.ToString()
            }).ToList();
            eligibleCategoriesList.Add(new SelectListItem { Text = "-- brak --", Value = string.Empty, Selected = true });
            return eligibleCategoriesList;
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            var model = new CreateCategoryViewModel
            {
                EligibleCategories = GetCategories()
            };
            return View(model);
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,ParentCategoryID")] CreateCategoryViewModel model)
        {
            if(model.ParentCategoryID != null)
            {
                var ParentCategory = Db.Categories.Find(model.ParentCategoryID);
                if (ParentCategory == null || (ParentCategory.RecordOwnerID != null && ParentCategory.RecordOwnerID != UserID))
                {
                    ModelState.AddModelError(string.Empty, "Nieprawidłowa kategoria nadrzędna!");
                }
            }
            if (ModelState.IsValid)
            {
                var category = new Category
                {
                    Name = model.Name,
                    ParentCategoryID = model.ParentCategoryID,
                    RecordOwnerID = UserID
                };
                Db.Categories.Add(category);
                Db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.EligibleCategories = GetCategories();
            return View(model);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = Db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentCategoryID = new SelectList(Db.Categories, "ID", "Name", category.ParentCategoryID);
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,ParentCategoryID,RecordOwnerID")] Category category)
        {
            if (ModelState.IsValid)
            {
                Db.Entry(category).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentCategoryID = new SelectList(Db.Categories, "ID", "Name", category.ParentCategoryID);
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = Db.Categories.Find(id);
            if (category == null || category.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = Db.Categories.Find(id);
            if (category == null || category.RecordOwnerID != UserID)
            {
                return HttpNotFound();
            }
            Db.Categories.Remove(category);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
