﻿using Microsoft.AspNet.Identity;
using PersonalBudget.Models;
using System.Web.Mvc;
using System.Web.Routing;

namespace PersonalBudget.Controllers.Abstract
{
    public class AbstractDatabaseController : Controller
    {
        protected ApplicationDbContext Db = new ApplicationDbContext();

        private string userID;
        private ApplicationUser userData;

        protected string UserID
        {
            get
            {
                if (userID == null)
                {
                    userID = User.Identity.GetUserId();
                }
                return userID;
            }
        }
        protected ApplicationUser UserData
        {
            get
            {
                if(userData == null)
                {
                    userData = Db.Users.Find(UserID);
                }
                return userData;
            }
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}