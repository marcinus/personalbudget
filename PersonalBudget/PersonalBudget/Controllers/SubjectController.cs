﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PersonalBudget.Models.ViewModels;
using PersonalBudget.Controllers.Abstract;
using PersonalBudget.Models.DbModels;

namespace PersonalBudget.Controllers
{
    [Authorize(Roles = "User")]
    public class SubjectController : AbstractDatabaseController
    {
        // GET: Subject
        public ActionResult Index()
        {

            var subjects = Db.Subjects.Where(s => s.EntryOwnerID == UserID).Include(s => s.Address).Include(s => s.EntryOwner).Include(s => s.RealSubject);
            return View(subjects.ToList());
        }

        // GET: Subject/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = Db.Subjects.Find(id);
            if (subject == null || subject.EntryOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // GET: Subject/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subject/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Street,PlaceNumber,PostalCode,City,Country")] SubjectAndAddressCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Subject subject = Subject.CreateFromViewModel(model);
                subject.EntryOwnerID = UserID;
                
                Db.Subjects.Add(subject);
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Subject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Subject subject = Db.Subjects.Find(id); 

            if (subject == null || subject.EntryOwnerID != UserID)
            {
                return HttpNotFound();
            }

            return View(SubjectAndAddressUpdateViewModel.FromModel(subject));
        }

        // POST: Subject/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubjectID, Name,Street,PlaceNumber,PostalCode,City,Country")] SubjectAndAddressUpdateViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Subject subject = Db.Subjects.Find(model.SubjectID);

            if (subject == null || subject.EntryOwnerID != UserID)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                subject.UpdateByViewModel(model);

                Db.Entry(subject).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Subject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = Db.Subjects.Find(id);
            if (subject == null || subject.EntryOwnerID != UserID)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Subject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject subject = Db.Subjects.Find(id);

            if (subject == null || subject.EntryOwnerID != UserID)
            {
                return HttpNotFound();
            }

            Db.Subjects.Remove(subject);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
